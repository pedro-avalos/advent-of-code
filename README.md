<div align="center">
    <img src="images/advent-of-code.gif">
    <h1>Advent of Code</h1>
</div>

This repository contains my solutions for [Advent of Code](https://adventofcode.com/) (coding challenges that release every year on December 1st).
